% LaTeX Beamer Slides for a C++ Introduction course

The slides here present a collection that might be useful for
people assembling course material for a C++ introduction.

The slides are deliberatly stored in single files, to make it
easy to put together a presentation tailored to specific needs.
The concrete [slides/intro-c++](slides/intro-c++.tex) is only provided
as an example.

Also the beamer template is provided as example only, to provide a quick start.
However, the resulting PDF (that's provided for reference as well)
is probably too ugly to present in real life.

The hope with this repository is to have a place where people can
work together to improve the quality of lecture material for
teaching C++.

The slides are generally provided under a CC BY-SA license,
but please see [slides/LICENSE.txt](slides/LICENSE.txt) for details.
