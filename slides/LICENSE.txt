% License for the slides in this repository

Except where otherwise stated, this work is licensed under a
[Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/).

For the files in the subdirectory beamer except tikz-uml.sty
and the file Makefile, the copyright and related rights are waived via
[CC0](https://creativecommons.org/publicdomain/zero/1.0/).

If you use the slides with your own beamer templates and LaTeX styles
to create a presentation, these beamer templates and LaTeX styles
(and any images or logos they may contain) are not considered derived
work of this work.

However, if you add new slides, the contents of these new slides
is considered part of derived work and need to be licensed under
terms in conformance with CC BY-SA.  If you want to avoid this
(e.g. if you want to give additional information on how to
access some resources for exercises, but don't want to publish
this information to the general public), just put such slides
into a separate PDF.
